import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class SolarShop extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public state: boolean

  @column()
  public powerOutput: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
