import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Customer extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public firstName: string

  @column()
  public lastName: string

  @column()
  public birthDate: DateTime

  @column()
  public gender: string

  @column()
  public profession: string

  @column()
  public subscriptionDate: DateTime

  @column()
  public userName: string

  @column()
  public password: string

  @column()
  public cardNumber: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
