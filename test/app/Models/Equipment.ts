import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Equipment extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public powerOutput: number

  @column()
  public powerInput: number

  @column()
  public measurmentUnit: string

  @column()
  public state: boolean

  @column()
  public equipmentTypeId: number

  @column()
  public equipmentFamilyId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
