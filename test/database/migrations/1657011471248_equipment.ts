import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'equipment'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.double('power_output')
      table.double('power_input')
      table.string('measurment_unit')
      table.boolean('state')
      table.integer('equipment_type_id').unsigned()
      table.integer('equipment_family_id').unsigned()
      table.foreign('equipment_type_id').references('id').inTable('equipment_types').onDelete('CASCADE')
      table.foreign('equipment_family_id').references('id').inTable('equipment_families').onDelete('CASCADE')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
